package com.supplierrisk.supplierrisk.dto;

import java.io.Serializable;
/**
 * 
 * @author joydeep.chakraborty
 *
 */
public class ErrorDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
