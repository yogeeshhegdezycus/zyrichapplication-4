userAuthenticationQuery|SELECT USERID,LOGINNAME,PASSWORD from USERS where LOGINNAME=? order by USERID desc
userRoleIdentificationQuery|SELECT ROLE FROM ZYRICH_ROLES WHERE USERNAME=?
getTableUserDBConn|select url,port,sid,username,password from ASA_DBCONFIG where userid=?
getIndexUnique|select indexname from ZYRICH_INDEXCONFIG
getIndexDetailQuery|select INDEXNAME, INDEXPATH, DATASOURCE, TABLENAME, IDCOLUMN, VENDORCOLUMN, COUNTRYCOLUMN, ZIPCODECOLUMN, ADDRESSCOLUMN,  TAXIDCOLUMN, SALESCOLUMN from ZYRICH_INDEXCONFIG where INDEXNAME=?
getavailableIndexQuery|select INDEXNAME from ZYRICH_INDEXCONFIG
getIndexQuery|select INDEXNAME, INDEXPATH, DATASOURCE, TABLENAME, IDCOLUMN, VENDORCOLUMN, ADDRESSCOLUMN, ZIPCODECOLUMN, COUNTRYCOLUMN, TAXIDCOLUMN, SALESCOLUMN, POBOXEXTRACTIONFEATURE, STATUS, POBOXCOLUMN from ZYRICH_INDEXCONFIG where INDEXNAME=?
getavailableTableQuery|select tname from tab
getavailableColumnQuery|select column_name from cols where table_name=?
getavailableIndexes|select INDEXNAME, INDEXPATH, DATASOURCE, TABLENAME, IDCOLUMN, VENDORCOLUMN, ADDRESSCOLUMN, ZIPCODECOLUMN, COUNTRYCOLUMN, TAXIDCOLUMN, SALESCOLUMN,POBOXCOLUMN, POBOXEXTRACTIONFEATURE  from ZYRICH_INDEXCONFIG where STATUS='AVAILABLE' and INDEXNAME= ?
getavailableIndexName|select INDEXNAME from ZYRICH_INDEXCONFIG 
updateIndexDetailQuery|update ZYRICH_INDEXCONFIG set INDEXNAME=? ,INDEXPATH=?, DATASOURCE=?, TABLENAME=?, IDCOLUMN=?, VENDORCOLUMN=?, ADDRESSCOLUMN=?, ZIPCODECOLUMN=?, COUNTRYCOLUMN=?, TAXIDCOLUMN=?, SALESCOLUMN=? where INDEXNAME=?
deleteIndexQuery|delete  from ZYRICH_INDEXCONFIG where INDEXNAME=?
newIndexQuery|insert into ZYRICH_INDEXCONFIG (INDEXPATH, DATASOURCE, TABLENAME, IDCOLUMN, VENDORCOLUMN, ADDRESSCOLUMN, ZIPCODECOLUMN, COUNTRYCOLUMN, TAXIDCOLUMN, SALESCOLUMN,POBOXCOLUMN, POBOXEXTRACTIONFEATURE,STATUS,INDEXNAME) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)
registerStatusQuery|insert into ZYRICH_STATUSTABLENEW  (JOBNAME, USERNAME, STARTDATE, STATUS ) values (?,?,?,?)
statusPopulateQuery|select JOBNAME, USERNAME, STARTDATE, ENDDATE, STATUS, DESCRIPTION from ZYRICH_STATUSTABLENEW where USERNAME=? order by STARTDATE desc
statusPopulateQueryIndividual|select JOBNAME, USERNAME, STARTDATE, ENDDATE, STATUS, DESCRIPTION from ZYRICH_STATUSTABLENEW where USERNAME=? and JOBNAME=?
statusPopulateActiveQuery| select JOBNAME, USERNAME, STARTDATE, ENDDATE, STATUS, DESCRIPTION from ZYRICH_STATUSTABLENEW where STATE='ACTIVE' order by STARTDATE asc 
statusUpdateQuery|update ZYRICH_STATUSTABLENEW set status=?, enddate=?  where JOBNAME=? and USERNAME=?
stateUpdateQuery|update ZYRICH_STATUSTABLENEW set state=? where JOBNAME=? and USERNAME=?
getUserDBConnectionQuery|select USERNAME, PASSWORD, URL, PORT, SID from ASA_DBCONFIG where USERID=?
emptyTagQuery|insert into zyrich_tags values (?,?,empty_clob())
getFieldTagQuery|select fields from zyrich_tags where tagname=? and indexname=?
updateTagQuery|update zyrich_tags set fields=? where  tagname=? and indexname=? 
getAvailableTagsQuery|select tagname,indexname,fields from zyrich_tags where indexname=?
